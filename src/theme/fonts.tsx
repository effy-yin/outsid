import { Global } from '@emotion/react';

const Fonts = () => (
  <Global
    styles={`
      @font-face {
        font-family: 'Digitype';
        src: url('./fonts/Digitype-Studio-MAINLUX-Regular.otf') format('opentype');
      }
      `}
  />
);

export default Fonts;
