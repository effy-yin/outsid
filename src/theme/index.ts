import { extendTheme, ThemeConfig } from '@chakra-ui/react';

const config: ThemeConfig = {
  // initialColorMode: 'dark',
  useSystemColorMode: false,
};

const colors = {
  brand: '#5BE8C1',
  textPrimary: '#333',
  textSecondary: '#666',
  gray: {
    800: '#000',
  },
};

const fonts = {
  body: 'Digitype'
};

const theme = extendTheme({ config, colors, fonts });
export default theme;
