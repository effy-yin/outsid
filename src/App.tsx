import React, { useEffect } from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';
import { Box, Flex, useColorMode } from '@chakra-ui/react';
import Header from 'components/Header';
import Footer from 'components/Footer';

import Home from 'pages/Home';
import Collection from 'pages/Collection';
import Governance from 'pages/Governance';
import Shop from 'pages/Shop';

function App() {
  let location = useLocation();
  const { colorMode, toggleColorMode } = useColorMode();

  const changeColorMode = () => {
    if (location.pathname === '/') {
      if (colorMode === 'light') toggleColorMode();
    } else if (colorMode === 'dark') toggleColorMode();
  };

  useEffect(changeColorMode, []); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(changeColorMode, [location]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Flex height='100%' direction='column'>
      <Header />

      <Box flex='1 0 auto' overflow='hidden' mt={{ base: '60px', md: '68px' }}>
        <Box width='100%' maxWidth='8xl' mx='auto' paddingX={4}>
          <Switch>
            <Route path='/collection'>
              <Collection />
            </Route>
            <Route path='/governance'>
              <Governance />
            </Route>
            <Route path='/shop'>
              <Shop />
            </Route>
            <Route path='/'>
              <Home />
            </Route>
          </Switch>
        </Box>
      </Box>
      <Box flexShrink={0}>
        <Footer />
      </Box>
    </Flex>
  );
}

export default App;
