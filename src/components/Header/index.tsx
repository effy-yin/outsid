import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import {
  Box,
  Flex,
  Button,
  Image,
  Drawer,
  DrawerOverlay,
  DrawerHeader,
  DrawerBody,
  DrawerContent,
  useMediaQuery,
  useColorModeValue,
  useDisclosure,
} from '@chakra-ui/react';
import { HamburgerIcon } from '@chakra-ui/icons';
import Cart from 'assets/images/cart.svg';

function Header() {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const bg = useColorModeValue('#fff', '#000');
  const bw = useColorModeValue(0, '1px');

  const [isBase] = useMediaQuery(['(max-width: 768px)']);
  const activeStyle = { color: '#5BE8C1' };

  return (
    <>
      <Flex
        as='header'
        position='fixed'
        left='0'
        right='0'
        height={{ base: '60px', md: '68px' }}
        boxShadow='base'
        borderBottomWidth={bw}
        borderBottomColor='gray.500'
        bg={bg}
        zIndex={999}
        fontWeight='semibold'
        justifyContent='center'>
        <Flex justifyContent='space-between' alignItems='center' width='100%' maxWidth='8xl'>
          <Box w='180px' textAlign='left' fontSize={{ base: '20px', md: '30px' }} px='4'>
            <Link to='/'>OUTSID</Link>
            <HamburgerIcon
              display={{ base: 'inline', md: 'none' }}
              w='5'
              h='5'
              ml={2}
              mb={1}
              cursor='pointer'
              onClick={onOpen}
            />
          </Box>

          <Box display={{ base: 'none', md: 'flex' }}>
            <NavLink exact to='/' activeStyle={activeStyle}>
              <Box mr='10'>Home</Box>
            </NavLink>
            <NavLink to='/collection' activeStyle={activeStyle}>
              <Box mr='10'>Collection</Box>
            </NavLink>
            <NavLink exact to='/governance' activeStyle={activeStyle}>
              <Box mr='10'>Governance</Box>
            </NavLink>
            <NavLink exact to='/shop' activeStyle={activeStyle}>
              <Box mr='10'>Shop</Box>
            </NavLink>
          </Box>
          <Flex justifyContent='space-between' width={{ base: '160px', md: '192px' }} px='4'>
            <Button size={isBase ? 'xs' : 'sm'} rounded='lg' bg='brand'>
              Connect Wallet
            </Button>
            <Image src={Cart} w={{ base: 4, md: 6 }} />
          </Flex>
        </Flex>
      </Flex>
      <Drawer placement='top' onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent textAlign='center'>
          <DrawerHeader borderBottomWidth='1px'>OUTSID</DrawerHeader>
          <DrawerBody>
            <Box>
              <NavLink exact to='/' activeStyle={activeStyle}>
                <Box my='3'>Home</Box>
              </NavLink>
              <NavLink to='/collection' activeStyle={activeStyle}>
                <Box my='3'>Collection</Box>
              </NavLink>
              <NavLink exact to='/governance' activeStyle={activeStyle}>
                <Box my='3'>Governance</Box>
              </NavLink>
              <NavLink exact to='/shop' activeStyle={activeStyle}>
                <Box my='3'>Shop</Box>
              </NavLink>
            </Box>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default Header;
