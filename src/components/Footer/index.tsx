import React from 'react';
import { Box, Flex, Button } from '@chakra-ui/react';

function Footer() {
  return (
    <Flex as='footer' flexDirection='column' alignItems='center' py='3' height='360px'>
      <a href='https://www.daosquare.io/' target='_blank' rel='noreferrer'>
        <Button variant='outline' size='md' rounded='lg' mb='3' display='block' width='200px'>
          What is Outsid
        </Button>
      </a>
      <a href='https://twitter.com/DAOSquare' target='_blank' rel='noreferrer'>
        <Button variant='outline' size='md' rounded='lg' mb='3' display='block' width='200px'>
          Twitter
        </Button>
      </a>
      <a href='https://discord.gg/JngTE8xMgX' target='_blank' rel='noreferrer'>
        <Button variant='outline' size='md' rounded='lg' mb='3' display='block' width='200px'>
          Discord
        </Button>
      </a>
      <Box height='60px' width='0' borderRight='1px' borderColor='gray.500' my='3'></Box>
      <Box fontSize='14px'>
        Incubated by{' '}
        <Box as='span' color='brand'>
          DAOSquare
        </Box>
      </Box>
    </Flex>
  );
}

export default Footer;
