import React from 'react';
import { Box, Image, Center, Flex, Text } from '@chakra-ui/react';
import Love from 'assets/images/love.svg';
import Snow from 'assets/images/snow.svg';
import Lock from 'assets/images/lock.svg';
import Eth from 'assets/images/eth.svg';

export interface CardProps {
  id: number;
  author: string;
  title: string;
  image: string;
  bid: number;
  like: number;
  isHidden?: boolean;
}

function Card({ author, title, image, bid, like, id, isHidden }: CardProps) {
  return (
    <Box
      visibility={isHidden ? 'hidden' : 'visible'}
      w={{ base: '90%', sm: '49%', md: '32%', lg: '24%', xl: '19%' }}
      h='428px'
      float='left'
      border='1px solid #EEEEEE'
      p={5}
      my={2}
      boxShadow='0px 2px 4px rgba(123, 70, 0, 0.1)'
      rounded='md'
      overflow='hidden'>
      <Flex justifyContent='space-between' w='100%' mb={2} color='textSecondary'>
        <Box marginTop={-1}>...</Box>
        <Flex>
          <Image src={Love} mr={1} w={5} />
          <Text fontSize={14}>{like}</Text>
        </Flex>
      </Flex>
      <Box w='100%'>
        <Center h='280px' objectFit='cover' w='100%' mx='auto'>
          <Image rounded='md' src={image} maxH='280px' />
        </Center>
      </Box>
      <Box>
        <Text fontSize={12} mt={2}>
          {author}
        </Text>
        <Text fontSize={14} fontWeight='bold'>
          {title}
        </Text>
        <Flex justifyContent='space-between' mt={3}>
          <Image src={bid === 0 ? Snow : Lock} w={4} />

          {bid !== 0 && (
            <Text fontSize='14px'>
              <Text color='textSecondary' as='span'>
                Last
              </Text>
              <Image src={Eth} ml={1} w={4} display='inline-block' />
              {bid}
            </Text>
          )}
        </Flex>
      </Box>
    </Box>
  );
}

export default Card;
