import React, { useState, useEffect } from 'react';
import { Box, Flex, Spinner, useMediaQuery } from '@chakra-ui/react';
import Card from 'components/Card';
import axios from 'axios';
import { useQuery } from 'react-query';

const nftUrl =
  'https://api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=50&owner=0x9ac9c636404C8d46D9eb966d7179983Ba5a3941A';
const excludeIds = [37829766, 31526005, 25541252, 36257126, 25522726, 23004406, 22268860, 19534725, 16455652];

function Collection() {
  const getNfts = async () => await axios.get(nftUrl);

  const query: { data: any; status: string } = useQuery('nfts', getNfts, {
    staleTime: 60 * 1000,
    cacheTime: 60 * 1000 * 10,
  });

  const [mdLg, lgXl, xl] = useMediaQuery([
    '(min-width: 768px) and (max-width: 992px)',
    '(min-width: 992px) and (max-width: 1280px)',
    '(min-width: 1280px) ',
  ]);

  let [cardData, setCardData] = useState([]);
  useEffect(() => {
    if (query.status !== 'success') return;
    if (cardData.length > 0) return;

    let data = query.data.data.assets.filter((d: any) => {
      return excludeIds.indexOf(d.id) === -1 && d.image_preview_url.indexOf('mp4') === -1;
    });
    data = data.map((d: any) => ({
      id: d.id,
      title: d.name,
      author: d.owner.user.username,
      image: d.image_preview_url,
      bid: 0,
      like: 0,
    }));
    setCardData(data);
  }, [query]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (cardData.length === 0) return;

    let data: object[] = [...cardData];
    if (mdLg && data.length % 3 !== 0) {
      if (data.length % 3 === 1) {
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
      } else if (data.length % 3 === 2) {
        data.push({ ...data[0], isHidden: true });
      }
    } else if (lgXl && data.length % 4 !== 0) {
      if (data.length % 4 === 1) {
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
      } else if (data.length % 4 === 2) {
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
      } else if (data.length % 4 === 3) {
        data.push({ ...data[0], isHidden: true });
      }
    } else if (xl && data.length % 5 !== 0) {
      if (data.length % 5 === 1) {
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
      } else if (data.length % 5 === 2) {
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
      } else if (data.length % 5 === 3) {
        data.push({ ...data[0], isHidden: true });
        data.push({ ...data[0], isHidden: true });
      } else if (data.length % 5 === 4) {
        data.push({ ...data[0], isHidden: true });
      }
    }
  }, [mdLg, lgXl, xl, cardData]);

  return (
    <Box>
      <Box textAlign='center' maxWidth='2xl' mx='auto' py={10} fontSize={{ base: 16, md: 20 }}>
        <Box>These are collections of Outsid.</Box>
        <Box>You can collect one to submit a proposal to create some physical items.</Box>
      </Box>

      {query.status === 'loading' && (
        <Box textAlign='center' paddingTop='20'>
          <Spinner color='brand' size='lg' />
        </Box>
      )}

      {query.status === 'success' && (
        <Flex wrap='wrap' justifyContent={{ base: 'center', sm: 'space-between' }} mb={20}>
          {cardData.map((d, i) => (
            <Card key={i} {...d} />
          ))}
        </Flex>
      )}
    </Box>
  );
}

export default Collection;
