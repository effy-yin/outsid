import React from 'react';
import { Box, Text, Flex, Image } from '@chakra-ui/react';
import Head from 'assets/images/head.png';

function Home() {
  return (
    <Box width='100%'>
      <Box display={{ base: 'block', md: 'flex' }} justifyContent='space-around' alignItems='center' py={16}>
        <Box
          width={{ base: '100%', md: '55%' }}
          fontSize={{ base: '24px', md: '28px', lg: '36px', xl: '48px' }}
          textAlign='center'
          mb='10'>
          <Box>Collaborate in the virtual world</Box>
          <Box>to make the real world better.</Box>
        </Box>
        <Flex width={{ base: '100%', md: '45%' }} justifyContent='center'>
          <Image src={Head} maxW='80%' />
        </Flex>
      </Box>
      <Box position='relative' h='88px' mb='20'>
        <Box
          color='brand'
          fontSize={{ base: '32px', md: '52px' }}
          fontWeight='bold'
          textTransform='uppercase'
          height='88px'
          lineHeight='88px'
          overflow='hidden'
          borderTopWidth='1px'
          borderBottomWidth='1px'
          borderColor='gray.600'
          width='2000px'
          marginLeft='-400px'>
          <Box>
            <Text
              position='absolute'
              whiteSpace='nowrap'
              willChange='transform'
              animation='marquee 20s linear infinite;'>
              outsid guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outsid guild
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;community brand outsid guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community
              brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outsid guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community
              brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outsid guild &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;community brand outsid
              guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outsid
              guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outsid guild
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;community brand outsid guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community
              brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outsid guild&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community
              brand&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </Text>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default Home;
